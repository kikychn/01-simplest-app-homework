package com.twuc.webApp.model;

import java.util.ArrayList;
import java.util.List;

public class Calculate {
    private Integer operandLeft;
    private Integer operandRight;
    private String operation;
    private Integer expectedResult;

    public Calculate() {
    }

    public Calculate(Integer operandLeft, Integer operandRight, String operation) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
    }

    public Calculate(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    public String calc() {
        List<String> numberArray = new ArrayList<>();
        for (int i = this.operandLeft; i <= this.operandRight; ++i) {
            StringBuilder singleLine = new StringBuilder();
            for (int j = this.operandLeft; j <= i; ++j) {
                if (this.operation.equals("*")) {
                    singleLine.append(i).append("*").append(j).append("=").append(i * j).append("\t");
                }
                if (this.operation.equals("+")) {
                    singleLine.append(i).append("+").append(j).append("=").append(i + j).append("\t");
                }

            }
            numberArray.add(singleLine.toString());
        }
        return String.join("\n", numberArray);
    }

    public Correct calcWithCorrect(Calculate calculate) {
        int result = 0;
        switch (calculate.getOperation()) {
            case "+":
                result = calculate.getOperandLeft() + calculate.getOperandRight();
                break;
            case "*":
                result = calculate.getOperandLeft() * calculate.getOperandRight();
                break;
            default:
                break;
        }
        return new Correct(result == calculate.getExpectedResult());
    }
}

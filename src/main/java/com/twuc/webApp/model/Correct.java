package com.twuc.webApp.model;

public class Correct {
    private boolean correct;

    public boolean isCorrect() {
        return correct;
    }

    public Correct(boolean correct) {
        this.correct = correct;
    }
}

package com.twuc.webApp.web;

import com.twuc.webApp.model.Calculate;
import com.twuc.webApp.model.Correct;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RestController
public class MathController {

    @GetMapping(value = "/api/tables/plus")
    public String getPlusTable(@RequestParam(defaultValue = "1") Integer operandLeft,
                               @RequestParam(defaultValue = "9") Integer operandRight) {
        return new Calculate(operandLeft, operandRight, "+").calc();
    }

    @GetMapping("/api/tables/multiply")
    public String getMultipyTable(@RequestParam(defaultValue = "1") @Min(1) Integer operandLeft, @RequestParam(defaultValue = "9") @Max(9) Integer operandRight) {
        Calculate calculate = new Calculate(operandLeft, operandRight, "*");
        return calculate.calc();
    }

    @PostMapping("/api/check")
    public ResponseEntity<Correct> check(@RequestBody Calculate calculate) {
        return ResponseEntity.status(OK)
                .contentType(APPLICATION_JSON)
                .body(calculate.calcWithCorrect(calculate));
    }

}

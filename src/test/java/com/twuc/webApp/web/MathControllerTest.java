package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.model.Calculate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class MathControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_200_as_status_code_when_request_special_api_endpoint() throws Exception {
        mockMvc.perform(get("/api/tables/plus"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"));
        mockMvc.perform(get("/api/tables/multiply"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"));

    }

    @Test
    void should_return_plus_table() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/plus")).andReturn();
        String expected = "1+1=2\t\n" +
                "2+1=3\t2+2=4\t\n" +
                "3+1=4\t3+2=5\t3+3=6\t\n" +
                "4+1=5\t4+2=6\t4+3=7\t4+4=8\t\n" +
                "5+1=6\t5+2=7\t5+3=8\t5+4=9\t5+5=10\t\n" +
                "6+1=7\t6+2=8\t6+3=9\t6+4=10\t6+5=11\t6+6=12\t\n" +
                "7+1=8\t7+2=9\t7+3=10\t7+4=11\t7+5=12\t7+6=13\t7+7=14\t\n" +
                "8+1=9\t8+2=10\t8+3=11\t8+4=12\t8+5=13\t8+6=14\t8+7=15\t8+8=16\t\n" +
                "9+1=10\t9+2=11\t9+3=12\t9+4=13\t9+5=14\t9+6=15\t9+7=16\t9+8=17\t9+9=18\t";
        assertEquals(expected, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void should_return_multipy_table() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/multiply")).andReturn();
        String expected = "1*1=1\t\n" +
                "2*1=2\t2*2=4\t\n" +
                "3*1=3\t3*2=6\t3*3=9\t\n" +
                "4*1=4\t4*2=8\t4*3=12\t4*4=16\t\n" +
                "5*1=5\t5*2=10\t5*3=15\t5*4=20\t5*5=25\t\n" +
                "6*1=6\t6*2=12\t6*3=18\t6*4=24\t6*5=30\t6*6=36\t\n" +
                "7*1=7\t7*2=14\t7*3=21\t7*4=28\t7*5=35\t7*6=42\t7*7=49\t\n" +
                "8*1=8\t8*2=16\t8*3=24\t8*4=32\t8*5=40\t8*6=48\t8*7=56\t8*8=64\t\n" +
                "9*1=9\t9*2=18\t9*3=27\t9*4=36\t9*5=45\t9*6=54\t9*7=63\t9*8=72\t9*9=81\t";
        assertEquals(expected, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void should_return_plus_table_between_3_and_5() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/plus")
                .param("operandLeft", "3")
                .param("operandRight", "5"))
                .andReturn();
        String expected = "3+3=6\t\n" +
                "4+3=7\t4+4=8\t\n" +
                "5+3=8\t5+4=9\t5+5=10\t";
        assertEquals(expected, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void should_return_multipy_table_between_3_and_5() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/multiply")
                .param("operandLeft", "3")
                .param("operandRight", "5"))
                .andReturn();
        String expected = "3*3=9\t\n" +
                "4*3=12\t4*4=16\t\n" +
                "5*3=15\t5*4=20\t5*5=25\t";
        assertEquals(expected, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void shoud_return_400_when_param_is_not_correct() throws Exception {
        mockMvc.perform(get("/api/tables/plus?operandLeft=-3&operandRight=5"))
                .andExpect(status().is(400));
        mockMvc.perform(get("/api/tables/multiply?operandLeft=5&operandRight=3"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_true_when_calculate_right() throws Exception {
        String operateJson1 = new ObjectMapper().writeValueAsString(new Calculate(1, 2, "+", 3));
        mockMvc.perform(post("/api/check")
                .content(operateJson1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect((content().contentType(MediaType.APPLICATION_JSON)))
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test
    void should_return_false_when_calculate_right() throws Exception {
        String operateJson2 = new ObjectMapper().writeValueAsString(new Calculate(1, 2, "*", 4));
        mockMvc.perform(post("/api/check")
                .content(operateJson2)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect((content().contentType(MediaType.APPLICATION_JSON)))
                .andExpect(jsonPath("$.correct").value(false));
    }
}